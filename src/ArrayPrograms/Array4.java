package ArrayPrograms;

public class Array4 {
    public static void main(String[] args) {
        String[] arr = {"S", "T", "A", "R"};
        String[] arr1 = {"T", "A", "R", "S"};
        String[] arr2 = {"A", "R", "S", "T"};
        String[] arr3 = {"R", "S", "T", "A"};
        // String[] arr4={"S T A R","T A R S","A R S T","R S T A"};
        for (String a : arr) {
            System.out.print(a);
        }
        System.out.println();
        for (String b : arr1) {
            System.out.print(b);
        }
        System.out.println();
        for (String c : arr2) {
            System.out.print(c);
        }
        System.out.println();
        for (String d : arr3) {
            System.out.print(d);
        }
    }
}
