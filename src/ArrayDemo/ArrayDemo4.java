package ArrayDemo;

import jdk.nashorn.internal.parser.JSONParser;

import java.util.Scanner;

public class ArrayDemo4 {
    public static void main(String[] args) {
        Scanner sc1 = new Scanner(System.in);
        System.out.println("Enter total no of Courses ");
        int size = sc1.nextInt();
        double[] marks = new double[size];
        System.out.println("Enter Marks");

        for (int a = 0; a < size; a++) {
            marks[a] = sc1.nextDouble();
        }
        double sum = 0;
        System.out.println("selected courses");
        for (int a = 0; a < size; a++) {
            sum+=marks[a];
        }
        double percent = sum / size;
        System.out.println("Percentage is :"+percent);
    }
}
