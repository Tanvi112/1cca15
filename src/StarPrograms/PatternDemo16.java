package StarPrograms;

public class PatternDemo16 {
    public static void main(String[] args) {
        int line=4;
        int num=4;
        int ch;

        for(int i=0;i<line;i++){
            ch=i+1;                          //int ch2=ch1;
            for(int j=0;j<num;j++){
                System.out.print(ch + "\t"); //sop(ch2++);
                ch++;
            }
            System.out.println();
                                              //ch++;
        }
    }
}


