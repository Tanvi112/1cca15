package PatternPdfQuestions;

public class Pattern3 {
    public static void main(String[] args) {
        int line=5;
        int star=1;

        for(int i=0;i<line;i++){
            for(int j=0;j<star;j++){
                System.out.print("*\t");
            }
            System.out.println();
            star++;
        }
    }
}
