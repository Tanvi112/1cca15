package SeriesPrograms;

public class Prime1 {
    public static void main(String[] args) {
        for (int j=1;j<=10000;j++) {
            int count = 0;
            int a = j;
            for (int i = 1; i <= a; i++)
                if (a % i == 0)
                    count++;

            if (count == 2)
                System.out.println(a+" number is prime");
        }
    }
}
