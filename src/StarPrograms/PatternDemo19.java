package StarPrograms;

public class PatternDemo19 {
    public static void main(String[] args) {
        int line=5;
        int num=5;
        int ch=1;

        for(int i=0;i<line;i++){
            for(int j=0;j<num;j++){
                if(i%2==0)
                    System.out.print(ch+"\t");
                else
                    System.out.print("*\t");
                }
            System.out.println();
            if(i%2==0)
                ch++;
            }
        }
    }

