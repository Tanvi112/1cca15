package StarPrograms;

public class PatternDemo34 {
    public static void main(String[] args) {
        int line = 7;
        int space = 3;
        int star = 1;
        //int ch=1;

        for (int i = 0; i < line; i++) {
            for (int k = 0; k < space; k++) {
                System.out.print(" ");
            }
            for (int j = 0; j < star; j++) {
                if (j == 0 || j == 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
            if (i <= 2) {
                star++;
                space--;
            } else {
                star--;
                space++;
            }
        }
    }
}
