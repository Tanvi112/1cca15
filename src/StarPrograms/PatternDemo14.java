package StarPrograms;

public class PatternDemo14 {
    public static void main(String[] args) {
        int line=5;
        int num=5;
        int ch=1;

        for(int i=0;i<line;i++){
            //int ch=1; print only 1
            for(int j=0;j<num;j++){
                System.out.print(ch + "\t");
            }
            System.out.println();
            ch++;
        }
    }
}
