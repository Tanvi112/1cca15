package SeriesPrograms;

public class Palindrome {
    public static void main(String[] args) {
        int a = 4554;
        int temp = a;
        int sum = 0;
        while (a != 0) {
            int r = a % 10;
            sum = sum * 10 + r;
            a = a / 10;
        }
        if (sum == temp) {
            System.out.print("Number is Palindrome");
        } else {
            System.out.print("Number is not palindrome");

        }
    }
}

