package StarPrograms;

public class PatternDemo22 {
    public static void main(String[] args) {
        int line=5;
        int num=1;
        int space=line-1;
        int ch;

        for(int i=0;i<line;i++){
            ch=1;
            for(int k=0;k<space;k++){
                System.out.print("    ");
            }
            for(int j=0;j<num;j++){
                System.out.print(ch + "\t");
                ch++;
            }
            System.out.println();
            space--;
            num++;

        }
    }
}
