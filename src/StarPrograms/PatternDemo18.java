package StarPrograms;

public class PatternDemo18 {
    public static void main(String[] args) {
        int line=4;
        int num=5;
        int ch=1;

        for(int i=0;i<line;i++){
            for(int j=0;j<num;j++){

                System.out.print(ch++ + "\t");
                if(ch>7){     //if(ch>'E')  { ch='A'}
                    ch=1;
                }
            }
            System.out.println();

        }
    }
}


