package StarPrograms;

public class PatternDemo17 {
    public static void main(String[] args) {
        int line=5;
        int num=5;
        int ch1=1;

        for(int i=0;i<line;i++){
            int ch2=1;
            for(int j=0;j<num;j++){
                System.out.print(ch2++ * ch1 +"\t");
            }
            System.out.println();
            ch1++;
        }
    }
}

