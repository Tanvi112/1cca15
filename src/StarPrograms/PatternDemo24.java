package StarPrograms;

public class PatternDemo24 {
    public static void main(String[] args) {
       int line=5;
       int star=1;
       int ch;
       int space=line-1;

       for(int i=0;i<line;i++){
           ch=1;
           for(int k=0;k<space;k++){
               System.out.print("    ");
           }
           for(int j=0;j<star;j++){
               System.out.print(ch +"\t");
               ch++;
           }
           System.out.println();
           star++;
           space--;
       }
    }
}
