package StarPrograms;

public class PatternDemo12 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;

        for (int i = 0;i < line; i++) {
            for (int j = 0;j < star; j++) {
                if ( i + j == 4 ||  i==j || i==0 ||j==0 || j==4 || i==4) {
                    System.out.print(" * ");
                } else
                    System.out.print("   ");
            }
            System.out.println();
        }
    }
}
