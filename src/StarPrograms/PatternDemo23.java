package StarPrograms;

public class PatternDemo23 {
    public static void main(String[] args) {
        int line=5;
        int num=1;
        int space=line-1;
        int ch1=5;

        for(int i=0;i<line;i++)
        {
            int ch2=ch1;
            for(int k=0;k<space;k++)
            {
                System.out.print("    ");
            }
            for(int j=0;j<num;j++)
            {
                System.out.print(ch2++ + "\t");
            }
            System.out.println();
            ch1--;
            space--;
            num++;

        }
    }
}
