package StarPrograms;

public class PatternDemo20 {
    public static void main(String[] args) {
        int line=5;
        int num=5;
        int ch;
        for(int i=0;i<line;i++) {
            ch = 1;
            char alpha = 'A';
            for (int j = 0; j < num; j++) {
                if (i % 2 == 0) {
                    System.out.print(alpha + "\t");
                    alpha++;
                } else
                    System.out.print(ch++ +"\t");
            }
            System.out.println();
        }

    }
}
