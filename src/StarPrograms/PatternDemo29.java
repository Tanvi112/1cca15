package StarPrograms;

public class PatternDemo29 {
    public static void main(String[] args) {
        int line=3;
        int star=3;
        int space=line-1;
        int ch;

        for(int i=0;i<line;i++){

            for(int k=0;k<space;k++){
                System.out.print("    ");
            }
            for(int j=0;j<star;j++){
                System.out.print( "*\t\t");

            }
            System.out.println();
            space++;
            star--;
        }
    }
}


