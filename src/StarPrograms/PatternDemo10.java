package StarPrograms;

public class PatternDemo10 {
    public static void main(String[] args) {
        int line = 5;
        int star = 5;

        for (int i = 0;i < line; i++) {
            for (int j = 0;j < star; j++) {
                if (i == j || i + j == 4 || i+j==1 || (i==0 && j==3) || (i==1 && j==4) || (i==3 && j==0) || (i==4 && j==1) || i+j==7) {
                    System.out.print(" * ");
                } else
                    System.out.print("   ");
            }
                System.out.println();
            }
        }
    }

