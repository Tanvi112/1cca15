package ArrayPrograms;

public class MissingNum {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 6, 7, 8, 9};
        int n=arr.length;
        //(n+1)*(n+2)/2
        int sum=((n+1)*(n+2))/2;
        for(int a: arr)
           sum-=a;
        System.out.println("Missing Number :"+sum);
    }
}
