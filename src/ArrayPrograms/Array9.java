package ArrayPrograms;
import java.util.Arrays;

public class Array9 {
    public static void main(String[] args) {
        int[] arr={1,2,3,4};
        int[] arr1={5,6,7,8,9};

        int n1=arr.length;
        int n2=arr1.length;
        int[] arr2=new int[n1+n2];
        int index=0;   //index value of arr2
        for(int a:arr){
            arr2[index]=a;
            index++;
        }
        for(int a:arr1){
            arr2[index]=a;
            index++;
        }
        for(int a:arr2) {
            System.out.print(a);
        }
       // System.out.println(Arrays.toString(arr2));

    }
}
