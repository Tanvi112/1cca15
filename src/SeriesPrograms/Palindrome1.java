package SeriesPrograms;

public class Palindrome1 {
    public static void main(String[] args) {

        for (int i = 1; i <= 10000; i++) {
            int sum = 0;
            int a = i;
            int temp = a;
            while (a != 0) {
                int r = a % 10;
                sum = sum * 10 + r;
                a = a / 10;
            }
            if (sum == temp) {
                System.out.println(sum);
            }
        }
    }
}

