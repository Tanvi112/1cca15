package SeriesPrograms;

public class FeboRecursion {
    static int n1 = 0;
    static int n2= 1;
    public static void main(String[] args) {
        int a=0;                                      //a=10;
        febo(a);
    }
      public  static void febo ( int a)
        {
            if (a <= 10) {                           //if(a >=0)
                System.out.print(n1+"\t");
                int sum = n1 + n2;
                n1 = n2;
                n2 = sum;
                a++;                                  //a--;
                febo(a);
            }
        }
}

