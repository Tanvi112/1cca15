package StarPrograms;

public class PatternDemo26 {
    public static void main(String[] args) {
        int line = 9;
        int star = 1;
        int space = line - 1;
        char ch = 'A';

        for (int i = 0; i < line; i++) {
            ch = 'A';
            for (int k = 0; k < space; k++) {
                System.out.print("    ");
            }
            for (int j = 0; j < star; j++) {
                System.out.print(ch + "\t\t");
                ch++;

            }
            System.out.println();
            if (i <= 3) {
                space--;
                star++;

            } else {
                space++;
                star--;
            }
        }
    }
}
