package StarPrograms;

public class PatternDemo27 {
    public static void main(String[] args) {
        int line=3;
        int star=1;
        int space=line-1;
        int ch;

        for(int i=0;i<line;i++){
             ch=1;
            for(int k=0;k<space;k++){
                System.out.print("    ");
            }
            for(int j=0;j<star;j++){
                System.out.print("*\t");
            }
            System.out.println();
            space--;
            star++;
        }
    }
}
